# ANTs-singularity


## Description
This repo is for ANTs Definition Files which can be build to singularity containers. They are built from the official github repo of [ANTsX](https://github.com/ANTsX/ANTs). For more information look at specific Definition File.


## Installation
Please mind that you might need a GPU to build.
How to build the singularity container: 
```
singularity build antsX.sif antsX.def
```
Where the X in antsX.sif and antsX.def shows the version of ANTs.


## Usage
Please consult the [official ANTs documentation](http://stnava.github.io/ANTsDoc/).


## Testing
Every tool of ANTs is tested while buildtime. 
For testing you can just use:
```
singularity test antsX.sif
```
A script will test ```antsBrainExtraction.sh``` as part of ANTs. Testdata is sourced [here](https://github.com/ntustison/antsBrainExtractionExample)


## License
Please mind the ANTs license:
```
Copyright (c) 2009-2013 ConsortiumOfANTS
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
  notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
  notice, this list of conditions and the following disclaimer in the
  documentation and/or other materials provided with the distribution.
3. Neither the name of the consortium nor the names of its contributors
  may be used to endorse or promote products derived from this software
  without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE CONSORTIUM AND CONTRIBUTORS ``AS IS'' AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
SUCH DAMAGE.
```
